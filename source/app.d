module app;

import std.stdio;
import std.string;

import mtk.ui;
//import mtk.containers;

int main(string[] argv)
{
    return 0;
}

version (unittest)
{
    import cairo.cairo;

    class TestWidget : Widget
    {
        double m_r;
        double m_g;
        double m_b;

        public this(double r, double g, double b)
        {
            m_r = r;
            m_g = g;
            m_b = b;
        }

        protected override void onExpose(Surface surface)
        {
            auto context = Context(surface);
            context.rectangle(0, 0, width, height);
            context.setSourceRGB(m_r, m_g, m_b);
            context.fill();
        }
    }

}

unittest
{
    auto wnd = new Window();

    wnd.content = new TestWidget(0.8, 0.8, 0.0);
    
    //auto st1 = wnd.createContent!Stack(Orientation.HORIZONTAL);
    //st1.appendFixed!TestWidget(50, 0.0, 0.8, 0.8);
    //st1.appendPlaceholder(50);
    
    //auto st2 = st1.appendRelative!Stack(1.0, Orientation.VERTICAL);
    //st2.appendRelative!TestWidget(1.0, 0.8, 0.8, 0.0);
    //st2.appendSpring(0.5);
    //st2.appendRelative!TestWidget(1.0, 0.8, 0.0, 0.8);

    auto st1 = new HorizontalStack();
    st1.addFixed(50, new TestWidget(0.0, 0.8, 0.8));
    st1.addPlaceholder(50);
    
    auto st2 = new VerticalStack();
    st2.addRelative(1.0, new TestWidget(0.8, 0.8, 0.0));
    st2.addSpring(0.5);
    st2.addRelative(1.0, new TestWidget(0.8, 0.0, 0.8));
    st1.addRelative(1.0, st2);

    wnd.content = st1;
    wnd.show();

    run();
}
