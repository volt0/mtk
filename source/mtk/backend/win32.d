module mtk.backend.win32;

version (Windows):

mixin template Backend()
{
    import core.runtime;
    import win32.windef;
    import win32.winbase;
    import win32.winuser;
    import win32.wingdi;
    import std.conv;
    import std.exception;
    import cairo.win32;

    alias HWND NativeWindow;

    alias cairo.cairo.RGB RGB; // conflicts with win32.wingdi.RGB

    private static immutable(HINSTANCE) instanceHandle;
    private static immutable(string) windowClassName = "mtk";

    shared static this()
    {
        instanceHandle = cast(immutable) GetModuleHandleA(null);

        WNDCLASS wcs;
        wcs.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
        wcs.lpfnWndProc = &wndProc;
        wcs.cbClsExtra = 0;
        wcs.cbWndExtra = 0;
        wcs.hInstance = cast(HINSTANCE) instanceHandle;
        wcs.hIcon = LoadIconA(null, IDI_APPLICATION);
        wcs.hCursor = LoadCursorA(null, IDC_ARROW);
        wcs.hbrBackground = cast(HBRUSH) COLOR_WINDOW;
        wcs.lpszMenuName = null;
        wcs.lpszClassName = windowClassName.toStringz();

        if(!RegisterClassA(&wcs))
        {
            throw new Error("This program requires Windows NT!");
        }
    }

    private enum
    {
        EMPTY_STRINGZ = "".toStringz(),
        DWL_IS_MOUSE_ENTERED = -21
    }

    private bool poll(ref int exitCode)
    {
        MSG msg;

        switch (GetMessageA(&msg, null, 0, 0))
        {
            case -1:
            {
                throw new Error("Unrecognised error in GetMessageA");
            }

            case 0:
            {
                exitCode = msg.wParam;
                return false;
            }

            default:
            {
                TranslateMessage(&msg);
                DispatchMessageA(&msg);
                return true;
            }
        }
    }

    extern (Windows)
    private LRESULT wndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
    {
        bool doOmitPostprocessing;

        template genFlagedCase(int msgId, string callback)
        {
            const string genFlagedCase = format(
                "case %d:{"
                    "window.%s.emit(cast(bool) wParam);"
                    "break;"
                "}",
                msgId,
                callback
                );
        }

        template genButtonCase(int msgId, ButtonCode code, bool state)
        {
            const string genButtonCase = format(
                "case %d:{"
                    "window.onButtonPress(ButtonCode.%s, %s, LOWORD(lParam), HIWORD(lParam));"
                    "break;"
                "}",
                msgId,
                code,
                state
                );
        }

        if (message == WM_CREATE)
        {
            if (hwnd in pool)
            {
                throw new Error("Dublicating HWND");
            }
            else
            {
                auto window = (cast(thisPtrWrapper *) ((cast(CREATESTRUCT *) lParam).lpCreateParams)).ptr;
                enforce(window);
                
                window.m_hwnd = hwnd;
                pool[hwnd] = window;
            }
        }

        if (hwnd && (hwnd in pool))
        {
            auto window = pool[hwnd];

            switch (message)
            {
                case WM_CREATE:
                {
                    //window.init();
                    break;
                }

                case WM_DESTROY:
                {
                    pool.remove(hwnd);

                    if (!pool.length)
                    {
                        PostQuitMessage(0);
                    }

                    break;
                }

                case WM_MOVE:
                case WM_SIZE:
                {
                    RECT rect;
                    GetWindowRect(hwnd, &rect);
                    window.onReshape(
                        rect.left,
                        rect.top,
                        rect.right - rect.left,
                        rect.bottom - rect.top
                        );

                    break;
                }

                case WM_PAINT:
                {
                    PAINTSTRUCT ps;
                    HDC hdc = BeginPaint(hwnd, &ps);

                    RECT rc;
                    GetClientRect(hwnd, &rc);
                    auto width = rc.right - rc.left;
                    auto height = rc.bottom - rc.top;

                    HDC buffer = CreateCompatibleDC(hdc);
                    HBITMAP newBmp = CreateCompatibleBitmap(hdc, width, height);
                    HBITMAP oldBmp = SelectObject(buffer, newBmp);
                    
                    auto surface = new Win32Surface(buffer);
                    window.onExpose(surface);
                    surface.finish();

                    BitBlt(hdc, 0, 0, width, height, buffer, rc.left, rc.top, SRCCOPY);

                    SelectObject(buffer, oldBmp);
                    DeleteObject(newBmp);
                    DeleteDC(buffer);

                    EndPaint(hwnd, &ps);

                    doOmitPostprocessing = true;
                    break;
                }

                case WM_SETFOCUS:
                {
                    window.focused.emit(true);
                    break;
                }

                case WM_KILLFOCUS:
                {
                    window.focused.emit(false);
                    break;
                }

                mixin(genFlagedCase!(WM_ENABLE, "enabled"));
                mixin(genFlagedCase!(WM_ACTIVATE, "activated"));

                case WM_CLOSE:
                {
                    doOmitPostprocessing = !window.onClose();
                    break;
                }

                case WM_SYSCOMMAND:
                {
                    switch (wParam)
                    {
                        case SC_RESTORE:
                        {
                            // NOTE: If window was maximized and then minimized, and then restored from taskbar,
                            // SC_RESTORE will be recieved, but not the SC_MAXIMIZE
                            window.windowStateChanged.emit(WindowState.DEFAULT);
                            break;
                        }

                        case SC_MAXIMIZE:
                        {
                            window.windowStateChanged.emit(WindowState.MAXIMIZED);
                            break;
                        }

                        case SC_MINIMIZE:
                        {
                            window.windowStateChanged.emit(WindowState.MINIMIZED);
                            break;
                        }
                
                        default:
                        {
                            break;
                        }
                    }

                    break;
                }

                case WM_CHAR:
                {
                    window.inputted.emit(to!string(cast(wchar) wParam));
                    break;
                }
                
                case WM_KEYUP:
                case WM_KEYDOWN:
                case WM_SYSKEYUP:
                case WM_SYSKEYDOWN:
                {
                    bool currentState = cast(bool) (lParam & 0x80000000);
                    bool previosState = cast(bool) (lParam & 0x40000000);
                    if (currentState == previosState)
                    {
                        KeyCode code = (wParam == VK_SHIFT)
                            ? win32KeyCodeConvTab[MapVirtualKey((lParam & 0xff0000) >> 16, 3)]
                            : win32KeyCodeConvTab[wParam & 0xff];

                        if (code)
                        {
                            if (lParam & 0x1000000)
                            {
                                switch (code)
                                {
                                    case KeyCode.CTRL: code = KeyCode.RIGHT_CTRL; break;
                                    case KeyCode.ALT: code = KeyCode.RIGHT_ALT; break;
                                    case KeyCode.ENTER: code = KeyCode.GR_ENTER; break;
                                    default: {}
                                }
                            }

                            window.onKeyPress(code, !currentState);
                        }
                    }
                    
                    break;
                }

                mixin(genButtonCase!(WM_LBUTTONDOWN, ButtonCode.LEFT,   true));
                mixin(genButtonCase!(WM_LBUTTONUP,   ButtonCode.LEFT,   false));
                mixin(genButtonCase!(WM_RBUTTONDOWN, ButtonCode.RIGHT,  true));
                mixin(genButtonCase!(WM_RBUTTONUP,   ButtonCode.RIGHT,  false));
                mixin(genButtonCase!(WM_MBUTTONDOWN, ButtonCode.MIDDLE, true));
                mixin(genButtonCase!(WM_MBUTTONUP,   ButtonCode.MIDDLE, false));

                case WM_MOUSEMOVE:
                {
                    if (!cast(bool) GetWindowLongA(hwnd, DWL_IS_MOUSE_ENTERED))
                    {
                        SetWindowLongA(hwnd, DWL_IS_MOUSE_ENTERED, 1);

                        TRACKMOUSEEVENT trackMouseSettings;
                        trackMouseSettings.cbSize = TRACKMOUSEEVENT.sizeof;
                        trackMouseSettings.dwFlags = TME_LEAVE;
                        trackMouseSettings.hwndTrack = hwnd;
                        trackMouseSettings.dwHoverTime = HOVER_DEFAULT;
                        TrackMouseEvent(&trackMouseSettings);

                        window.mouseEntered.emit();
                    }

                    window.onMouseMove(LOWORD(lParam), HIWORD(lParam));
                    break;
                }

                case WM_MOUSELEAVE:
                {
                    SetWindowLongA(hwnd, DWL_IS_MOUSE_ENTERED, 0);
                    window.mouseLeft.emit();
                    break;
                }

                case WM_MOUSEWHEEL:
                {
                    window.onWheel(WheelCode.VERTICAL, GET_WHEEL_DELTA_WPARAM(wParam));
                    break;
                }

                default:
                {
                    break;
                }
            }
        }

        return doOmitPostprocessing ? 0 : DefWindowProcA(hwnd, message, wParam, lParam);
    }

    private struct thisPtrWrapper
    {
        Window ptr;
    }

    private static immutable(KeyCode[256]) win32KeyCodeConvTab = [
        KeyCode.NONE, KeyCode.NONE, KeyCode.NONE,
        KeyCode.CANCEL,
        KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE,
        KeyCode.BACKSPACE,
        KeyCode.TAB,
        KeyCode.NONE, KeyCode.NONE,
        KeyCode.CLEAR,
        KeyCode.ENTER,
        KeyCode.NONE, KeyCode.NONE, 
        KeyCode.SHIFT, KeyCode.CTRL, KeyCode.ALT,
        KeyCode.PAUSE,
        KeyCode.CAPSLOCK,
        KeyCode.IME_HANGUL_OR_KANA,
        KeyCode.NONE,
        KeyCode.IME_JUNJA, KeyCode.IME_FINAL, KeyCode.IME_HANJA_OR_KANJI,
        KeyCode.NONE,
        KeyCode.ESCAPE,
        KeyCode.IME_CONVERT, KeyCode.IME_NONCONVERT, KeyCode.IME_ACCEPT, KeyCode.IME_MODECHANGE, 
        KeyCode.SPACE, 
        KeyCode.PAGE_UP, KeyCode.PAGE_DOWN,
        KeyCode.END, KeyCode.HOME,
        KeyCode.LEFT, KeyCode.UP, KeyCode.RIGHT, KeyCode.DOWN,
        KeyCode.SELECT,
        KeyCode.PRINT,
        KeyCode.EXECUTE,
        KeyCode.PRINT_SCREEN,
        KeyCode.INSERT, KeyCode.DELETE,
        KeyCode.HELP, 
        KeyCode.KEY_0 , KeyCode.KEY_1, KeyCode.KEY_2, KeyCode.KEY_3, KeyCode.KEY_4,
        KeyCode.KEY_5, KeyCode.KEY_6, KeyCode.KEY_7, KeyCode.KEY_8, KeyCode.KEY_9,
        KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE,
        KeyCode.KEY_A, KeyCode.KEY_B, KeyCode.KEY_C, KeyCode.KEY_D, KeyCode.KEY_E, KeyCode.KEY_F, KeyCode.KEY_G, KeyCode.KEY_H,
        KeyCode.KEY_I, KeyCode.KEY_J, KeyCode.KEY_K, KeyCode.KEY_L, KeyCode.KEY_M, KeyCode.KEY_N, KeyCode.KEY_O, KeyCode.KEY_P,
        KeyCode.KEY_Q, KeyCode.KEY_R, KeyCode.KEY_S, KeyCode.KEY_T, KeyCode.KEY_U, KeyCode.KEY_V, KeyCode.KEY_W, KeyCode.KEY_X,
        KeyCode.KEY_Y, KeyCode.KEY_Z,
        KeyCode.CMD, KeyCode.RIGHT_CMD,
        KeyCode.APPS,
        KeyCode.NONE,
        KeyCode.SLEEP, 
        KeyCode.GR_0, KeyCode.GR_1, KeyCode.GR_2, KeyCode.GR_3, KeyCode.GR_4,
        KeyCode.GR_5, KeyCode.GR_6, KeyCode.GR_7, KeyCode.GR_8, KeyCode.GR_9,
        KeyCode.GR_MUL, KeyCode.GR_ADD, KeyCode.GR_SEPARATOR, KeyCode.GR_SUB, KeyCode.GR_POINT, KeyCode.GR_DIV, 
        KeyCode.F1, KeyCode.F2, KeyCode.F3, KeyCode.F4, KeyCode.F5, KeyCode.F6, KeyCode.F7, KeyCode.F8,
        KeyCode.F9, KeyCode.F10, KeyCode.F11, KeyCode.F12, KeyCode.F13, KeyCode.F14, KeyCode.F15, KeyCode.F16, 
        KeyCode.F17, KeyCode.F18, KeyCode.F19, KeyCode.F20, KeyCode.F21, KeyCode.F22, KeyCode.F23, KeyCode.F24,
        KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, 
        KeyCode.NUMLOCK, KeyCode.SCROLLLOCK,
        KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE,
        KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, 
        KeyCode.SHIFT, KeyCode.RIGHT_SHIFT,
        KeyCode.CTRL, KeyCode.RIGHT_CTRL,
        KeyCode.ALT, KeyCode.RIGHT_ALT,
        KeyCode.BROWSER_BACK, KeyCode.BROWSER_FORWARD, KeyCode.BROWSER_REFRESH, KeyCode.BROWSER_STOP,
        KeyCode.BROWSER_SEARCH, KeyCode.BROWSER_FAVORITES, KeyCode.BROWSER_HOME,
        KeyCode.VOLUME_MUTE, KeyCode.VOLUME_DOWN, KeyCode.VOLUME_UP, 
        KeyCode.MEDIA_NEXT_TRACK, KeyCode.MEDIA_PREV_TRACK, KeyCode.MEDIA_STOP, KeyCode.MEDIA_PLAY_PAUSE,
        KeyCode.LAUNCH_MAIL, KeyCode.LAUNCH_MEDIA_SELECT, KeyCode.LAUNCH_APP1, KeyCode.LAUNCH_APP2,
        KeyCode.NONE, KeyCode.NONE,
        KeyCode.COLON, KeyCode.PLUS , KeyCode.COMMA, KeyCode.MINUS, KeyCode.POINT, KeyCode.QUESTION_MARK, KeyCode.TILDE,
        KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE,
        KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE,
        KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE,
        KeyCode.NONE, KeyCode.NONE,
        KeyCode.LEFT_BRACE, KeyCode.PIPE, KeyCode.RIGHT_BRACE, KeyCode.QUOTE, KeyCode.SECTION_SIGN, 
        KeyCode.NONE, KeyCode.NONE,
        KeyCode.LESS_GREATER,
        KeyCode.NONE, KeyCode.NONE,
        KeyCode.IME_PROCESSKEY,
        KeyCode.NONE,
        KeyCode.PACKET,
        KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, 
        KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE, KeyCode.NONE,
        KeyCode.ATTN, KeyCode.CRSEL, KeyCode.EXSEL, KeyCode.EREOF, KeyCode.PLAY, KeyCode.ZOOM, KeyCode.NONAME, KeyCode.PA1,
        KeyCode.CLEAR, KeyCode.NONE
    ];
}

mixin template WindowBackbone()
{
    private HWND m_hwnd;

    private final void spawn()
    {
        thisPtrWrapper wrapper;
        wrapper.ptr = this;

        auto hwnd = CreateWindowA(
            windowClassName.toStringz(), // window class name
            Runtime.args[0].toStringz(), // window caption
            WS_OVERLAPPEDWINDOW,         // window style
            CW_USEDEFAULT,               // initial x position
            CW_USEDEFAULT,               // initial y position
            CW_USEDEFAULT,               // initial x size
            CW_USEDEFAULT,               // initial y size
            null,                        // parent window handle
            null,                        // window menu handle
            cast(HINSTANCE) instanceHandle, // program instance handle
            &wrapper                     // creation parameters
            );

        SetWindowLongA(hwnd, DWL_IS_MOUSE_ENTERED, 0);
    }

    private final void nativeShow()
    {
        //ShowWindow(m_hwnd, SW_SHOWDEFAULT);
        ShowWindow(m_hwnd, SW_SHOW);
        UpdateWindow(m_hwnd);
    }

    private final void nativeHide()
    {
        ShowWindow(m_hwnd, SW_HIDE);
        UpdateWindow(m_hwnd);
    }

    private final void nativeClose()
    {
        SendMessage(m_hwnd, WM_CLOSE, 0, 0); 
    }

    private final void nativeMove(int x, int y)
    {
        SetWindowPos(m_hwnd, null, x, y, 0, 0, SWP_NOSIZE | SWP_NOZORDER);
    }

    private final void nativeResize(int width, int height)
    {
        SetWindowPos(m_hwnd, null, 0, 0, width, height, SWP_NOMOVE | SWP_NOZORDER);
    }

    //private final void nativeGetSize(ref int width, ref int height)
    //{
    //    RECT rect;
    //    GetWindowRect(m_hwnd, &rect);
    //    width = rect.right - rect.left;
    //    height = rect.bottom - rect.top;
    //}
    
}


/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

version(never):

mixin template WidgetBackboneImpl()
{
    private final void fork(Widget parent)
    {
        thisPtrWrapper wrapper;
        wrapper.ptr = this;

        auto hwnd = CreateWindowA(
            windowClassName.toStringz(),
            EMPTY_STRINGZ,
            WS_CHILD | WS_VISIBLE | WS_OVERLAPPED,
            0, 0, 100, 100,
            parent.m_hwnd,
            null,
            cast(HINSTANCE) instanceHandle,
            &wrapper
            );

        SetWindowLongA(hwnd, DWL_IS_MOUSE_ENTERED, 0);

        ShowWindow(m_hwnd, SW_SHOW);
        UpdateWindow(m_hwnd);
    }

    private final int implGetWidth()
    {
        RECT rect;
        GetWindowRect(m_hwnd, &rect);

        return rect.right - rect.left;
    }

    private final int implGetHeight()
    {
        RECT rect;
        GetWindowRect(m_hwnd, &rect);

        return rect.bottom - rect.top;
    }
    
    extern (Windows)
    private static LRESULT wndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
    {
        bool doOmitPostprocessing;

        enum genWindowEventHeader = 
            "auto window = cast(Window) widget;"
            "if (!window) break;"
            ;

        if (message == WM_CREATE)
        {
            if (hwnd in m_pool)
            {
                throw new Error("Dublicating HWND");
            }
            else
            {
                auto widget = (cast(thisPtrWrapper *) ((cast(CREATESTRUCT *) lParam).lpCreateParams)).ptr;
                enforce(widget);
                
                widget.m_hwnd = hwnd;
                m_pool[hwnd] = widget;
            }
        }

        if (hwnd && (hwnd in m_pool))
        {
            auto widget = m_pool[hwnd];

            switch (message)
            {
                case WM_CREATE:
                {
                    widget.init();
                    break;
                }

                case WM_DESTROY:
                {
                    m_pool.remove(hwnd);

                    if (!m_pool.length)
                    {
                        PostQuitMessage(0);
                    }

                    break;
                }

                case WM_PAINT:
                {
                    PAINTSTRUCT ps;
                    HDC hdc = BeginPaint(hwnd, &ps);

                    RECT rc;
                    GetClientRect(hwnd, &rc);
                    auto width = rc.right - rc.left;
                    auto height = rc.bottom - rc.top;

                    HDC buffer = CreateCompatibleDC(hdc);
                    HBITMAP newBmp = CreateCompatibleBitmap(hdc, width, height);
                    HBITMAP oldBmp = SelectObject(buffer, newBmp);
                    
                    auto surf = new Win32Surface(buffer);
                    auto ctx = Context(surf);

                    widget.onPaint(ctx);

                    surf.finish();
                    BitBlt(hdc, 0, 0, width, height, buffer, rc.left, rc.top, SRCCOPY);

                    SelectObject(buffer, oldBmp);
                    DeleteObject(newBmp);
                    DeleteDC(buffer);

                    EndPaint(hwnd, &ps);

                    doOmitPostprocessing = true;
                    break;
                }

                default:
                {
                    break;
                }
            }
        }

        return doOmitPostprocessing ? 0 : DefWindowProcA(hwnd, message, wParam, lParam);
    }

}

mixin template WindowBackboneImpl()
{
    private final void implSetWindowState(WindowState state)
    {
        switch (state)
        {
            case WindowState.HIDDEN:
            {
                ShowWindow(m_hwnd, SW_HIDE);
                break;
            }

            case WindowState.DEFAULT:
            {
                ShowWindow(m_hwnd, SW_RESTORE);

                if (implGetWindowState() != WindowState.DEFAULT)
                {
                    ShowWindow(m_hwnd, SW_RESTORE);
                }

                break;
            }

            case WindowState.MAXIMIZED:
            {
                ShowWindow(m_hwnd, SW_MAXIMIZE);
                break;
            }

            case WindowState.MINIMIZED:
            {
                ShowWindow(m_hwnd, SW_MINIMIZE);
                break;
            }

            default:
            {
                assert(0);
            }
        }
        
        UpdateWindow(m_hwnd);
    }

    private final WindowState implGetWindowState()
    {
        WINDOWPLACEMENT placement;
        placement.length = WINDOWPLACEMENT.sizeof;

        GetWindowPlacement(m_hwnd, &placement);

        switch (placement.showCmd)
        {
            case SW_HIDE:
            {
                return WindowState.HIDDEN;
            }

            case SW_RESTORE:
            case SW_SHOW:
            case SW_SHOWNA:
            case SW_SHOWNOACTIVATE:
            case SW_SHOWNORMAL:
            {
                return WindowState.DEFAULT;
            }

            case SW_MAXIMIZE:
            //case SW_SHOWMAXIMIZED:
            {
                return WindowState.MAXIMIZED;
            }

            case SW_MINIMIZE:
            case SW_SHOWMINIMIZED:
            case SW_SHOWMINNOACTIVE:
            {
                return WindowState.MINIMIZED;
            }

            default:
            {
                assert(0);
            }
        }
    }
}
