module mtk.containers;

import mtk.ui;
import cairo.cairo;

//import std.stdio;

//enum Orientation
//{
//    VERTICAL,
//    HORIZONTAL
//}

//class Stack : Container
//{
//    public this(Widget parent, Orientation orientation = Orientation.VERTICAL)
//    {
//        super(parent);
        
//        m_orientation = orientation;
//        resized.connect(&onResize);
//    }

//    public T appendRelative(T, A...)(double stratch, A args)
//    in
//    {
//        assert(stratch > 0);
//    }
//    body
//    {
//        auto child = createChild!T(this, args);

//        Node node;
//        node.item = child;
//        node.stratch = stratch;
//        m_nodes ~= node;
        
//        onResize(width, height);

//        return child;
//    }

//    public T appendFixed(T, A...)(int size, A args)
//    in
//    {
//        assert(size > 0);
//    }
//    body
//    {
//        auto child = createChild!T(this, args);

//        Node node;
//        node.isFixed = true;
//        node.item = child;
//        node.size = size;
//        m_nodes ~= node;
        
//        onResize(width, height);

//        return child;
//    }

//    public void appendSpring(double stratch)
//    in
//    {
//        assert(stratch > 0);
//    }
//    body
//    {
//        Node node;
//        node.stratch = stratch;
//        m_nodes ~= node;
        
//        onResize(width, height);
//    }

//    public void appendPlaceholder(int size)
//    in
//    {
//        assert(size > 0);
//    }
//    body
//    {
//        Node node;
//        node.isFixed = true;
//        node.size = size;
//        m_nodes ~= node;
        
//        onResize(width, height);
//    }

//    private void onResize(int width, int height)
//    {
//        double sStratch = 0.0;
//        double sSize = m_orientation ? width : height;
//        foreach (ref Node i; m_nodes)
//        {
//            if (i.isFixed)
//            {
//                sSize -= i.size;
//            }
//            else
//            {
//                sStratch += i.stratch;
//            }
//        }

//        double k = sSize / sStratch;
//        int soffs;
//        int foffs;

//        foreach (ref Node i; m_nodes)
//        {
//            foffs += i.isFixed ? i.size : cast(int) round(i.stratch * k);

//            if (i.item)
//            {
//                if (m_orientation)
//                {
//                    i.item.reshape(soffs, 0, foffs - soffs, height);
//                }
//                else
//                {
//                    i.item.reshape(0, soffs, width, foffs - soffs);
//                }
//            }

//            soffs = foffs;
//        }
//    }

//    private struct Node
//    {
//        bool isFixed;
//        Widget item;
//        double stratch;
//        int size;
//    }

//    private Orientation m_orientation;
//    private Node[] m_nodes;
//}