module mtk.ui;

import mtk.backend.win32;
import std.math;
import std.signals;
import std.string;
import cairo.cairo;

import std.stdio;

mixin Backend;

private Window[NativeWindow] pool;

int run()
{
    int result;
    while (poll(result))
    {
    }

    return result;
}

class Widget
{
    public this()
    {
    }

    public void show()
    {
        m_visible = true;
    }

    public void hide()
    {
        m_visible = false;
    }

    @property
    public int width()
    {
        return m_width;
    }
    
    @property
    public int height()
    {
        return m_height;
    }

    protected void resize(int width, int height)
    {
        m_width = width;
        m_height = height;
    }

    protected void onExpose(Surface surface)
    {
        auto context = Context(surface);
        context.rectangle(0, 0, m_width, m_height);
        context.setSourceRGB(0.0, 0.5, 0.5);
        context.fill();
    }

    //protected void updateDimensions()
    //{
    //    onReshape(m_x, m_y, m_width, m_height);
    //}

    private Widget m_parent;

    private int m_width;
    private int m_height;

    private bool m_visible;
}

class Window
{
    public this()
    {
        spawn();
    }

    @property
    public final Widget content()
    {
        return m_content;
    }

    @property
    public final void content(Widget widget)
    {
        m_content = widget;
    }

    @property
    public final int width()
    {
        return m_width;
    }
    
    @property
    public final int height()
    {
        return m_height;
    }

    public final void show()
    {
        nativeShow();
    }

    public final void hide()
    {
        nativeHide();
    }

    public final void close()
    {
        nativeClose();
    }

    //@property
    //public final void windowState(WindowState state)
    //{
    //    implSetWindowState(state);
    //}

    //@property
    //public final WindowState windowState()
    //{
    //    return implGetWindowState();
    //}

    public final void move(int x, int y)
    {
        nativeMove(x, y);
    }

    public final void resize(int width, int height)
    {
        nativeResize(width, height);
    }

    mixin Signal!(bool) enabled;
    mixin Signal!(bool) activated;
    mixin Signal!(bool) focused;
    mixin Signal!(int, int) moved;
    mixin Signal!(int, int) resized;

    mixin Signal!(string) inputted;
    mixin Signal!() mouseEntered;
    mixin Signal!() mouseLeft;
    mixin Signal!(int, int) clicked;
    mixin Signal!(int, int) doubleClicked;

    mixin Signal!(WindowState) windowStateChanged;

    protected void onExpose(Surface surface)
    {
        if (m_content)
        {
            m_content.onExpose(surface);
        }
        else
        {
            auto context = Context(surface);
            context.rectangle(0, 0, width, height);
            context.setSourceRGB(0.5, 0.5, 0.5);
            context.fill();
        }
    }

    private void onReshape(int x, int y, int width, int height)
    {
        m_width = width;
        m_height = height;
        m_x = x;
        m_y = y;

        if (m_content)
        {
            m_content.resize(width, height);
        }
    }

    protected bool onClose()
    {
        return true;
    }

    protected void onKeyPress(KeyCode code, bool state)
    {
    }

    protected void onButtonPress(ButtonCode code, bool state, int x, int y)
    {
    }

    protected void onMouseMove(int x, int y)
    {
    }

    protected void onWheel(WheelCode code, int delta)
    {
    }

    private int m_width;
    private int m_height;
    private int m_x;
    private int m_y;
    private Widget m_content;

    mixin WindowBackbone;
}

class Container : Widget
{
}

alias AbstractStack!true HorizontalStack;
alias AbstractStack!false VerticalStack;

class AbstractStack(bool isHorizontal) : Container
{
    public void addRelative(double stratch, Widget child)
    in
    {
        assert(child);
        assert(stratch > 0.0);
    }
    body
    {
        Node node;
        node.item = child;
        node.stratch = stratch;
        m_nodes ~= node;

        child.m_parent = this;
        resize(width, height);
    }

    public void addFixed(int size, Widget child)
    in
    {
        assert(size > 0);
    }
    body
    {
        Node node;
        node.isFixed = true;
        node.item = child;
        node.size = size;
        m_nodes ~= node;
        
        child.m_parent = this;
        resize(width, height);
    }

    public void addSpring(double stratch)
    in
    {
        assert(stratch > 0);
    }
    body
    {
        Node node;
        node.stratch = stratch;
        m_nodes ~= node;
        
        resize(width, height);
    }

    public void addPlaceholder(int size)
    in
    {
        assert(size > 0);
    }
    body
    {
        Node node;
        node.isFixed = true;
        node.size = size;
        m_nodes ~= node;
        
        resize(width, height);
    }

    protected override void resize(int width, int height)
    {
        super.resize(width, height);

        double sStratch = 0.0;
        double sSize = isHorizontal ? width : height;
        foreach (ref Node i; m_nodes)
        {
            if (i.isFixed)
            {
                sSize -= i.size;
            }
            else
            {
                sStratch += i.stratch;
            }
        }

        double k = sSize / sStratch;
        int soffs;
        int foffs;

        foreach (ref Node i; m_nodes)
        {
            foffs += i.isFixed ? i.size : cast(int) round(i.stratch * k);
            i.offset = soffs;

            if (i.item)
            {
                static if (isHorizontal)
                {
                    i.item.resize(foffs - soffs, height);
                }
                else
                {
                    i.item.resize(width, foffs - soffs);
                }
            }

            soffs = foffs;
        }
    }

    protected override void onExpose(Surface surface)
    {
        foreach (ref Node i; m_nodes)
        {
            if (i.item)
            {
                static if (isHorizontal)
                {
                    i.item.onExpose(Surface.createForRectangle(
                        surface,
                        Rectangle!double(i.offset, 0, i.item.width, i.item.height)
                        ));
                }
                else
                {
                    i.item.onExpose(Surface.createForRectangle(
                        surface,
                        Rectangle!double(0, i.offset, i.item.width, i.item.height)
                        ));
                }
            }
        }
    }

    private struct Node
    {
        Widget item;
        int offset;
        
        bool isFixed;
        double stratch;
        int size;
    }

    private Node[] m_nodes;
}

enum KeyCode
{
    NONE,

    SHIFT,
    CTRL,
    ALT,
    PAGE_UP, PAGE_DOWN,
    HOME, END,
    TAB,
    ESCAPE,
    INSERT, DELETE,
    CAPSLOCK,
    ENTER = 0x0d,

    RIGHT_SHIFT,
    RIGHT_CTRL,
    RIGHT_ALT,
    CMD,
    RIGHT_CMD,

    LEFT,
    UP,
    DOWN,
    RIGHT,

    APPS,
    PRINT_SCREEN,
    SCROLLLOCK,
    PAUSE,
    BACKSPACE,

    SPACE = 0x20,
    QUOTE = 0x22,
    PLUS = 0x2b,
    COMMA,
    MINUS,
    POINT,

    KEY_0 = 0x30,
    KEY_1, KEY_2, KEY_3, KEY_4, KEY_5,
    KEY_6, KEY_7, KEY_8, KEY_9,

    COLON = 0x3a,
    LESS_GREATER = 0x3c,
    QUESTION_MARK = 0x3f,

    KEY_A = 0x41,
    KEY_B, KEY_C, KEY_D, KEY_E, KEY_F,
    KEY_G, KEY_H, KEY_I, KEY_J, KEY_K,
    KEY_L, KEY_M, KEY_N, KEY_O, KEY_P,
    KEY_Q, KEY_R, KEY_S, KEY_T, KEY_U,
    KEY_V, KEY_W, KEY_X, KEY_Y, KEY_Z,

    SECTION_SIGN = 0x60,
    LEFT_BRACE = 0x7b,
    PIPE,
    RIGHT_BRACE,
    TILDE,

    NUMLOCK = 0x80,
    GR_0, GR_1, GR_2, GR_3, GR_4,
    GR_5, GR_6, GR_7, GR_8, GR_9,
    GR_DIV,
    GR_MUL,
    GR_SUB,
    GR_ADD,
    GR_ENTER,
    GR_POINT,
    GR_SEPARATOR,

    F1,  F2,  F3,  F4,  F5,
    F6,  F7,  F8,  F9,  F10,
    F11, F12, F13, F14, F15,
    F16, F17, F18, F19, F20,
    F21, F22, F23, F24,

    IME_HANGUL_OR_KANA,
    IME_JUNJA,
    IME_FINAL,
    IME_HANJA_OR_KANJI,
    IME_CONVERT,
    IME_NONCONVERT,
    IME_ACCEPT,
    IME_MODECHANGE,
    IME_PROCESSKEY,

    CANCEL,
    CLEAR,
    SELECT,
    PRINT,
    EXECUTE,
    HELP,
    SLEEP,
    PACKET,
    ATTN,
    CRSEL,
    EXSEL,
    EREOF,
    NONAME,
    PA1,

    BROWSER_BACK,
    BROWSER_FORWARD,
    BROWSER_REFRESH,
    BROWSER_STOP,
    BROWSER_SEARCH,
    BROWSER_FAVORITES,
    BROWSER_HOME,
    VOLUME_MUTE,
    VOLUME_DOWN,
    VOLUME_UP,
    MEDIA_NEXT_TRACK,
    MEDIA_PREV_TRACK,
    MEDIA_STOP,
    MEDIA_PLAY_PAUSE,
    LAUNCH_MAIL,
    LAUNCH_MEDIA_SELECT,
    LAUNCH_APP1,
    LAUNCH_APP2,
    PLAY,
    ZOOM
}

enum ButtonCode
{
    LEFT,
    RIGHT,
    MIDDLE,

    BUTTON4,
    BUTTON5
}

enum WheelCode
{
    VERTICAL,
    HORIZONTAL
}

enum WindowState
{
    HIDDEN,
    DEFAULT,
    MAXIMIZED,
    MINIMIZED
}
